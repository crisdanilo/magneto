# Magneto

Magneto Set of plugins based on decorators to build api rest quickly and semantically.

## Instalation
- Clone project
- Install dependencies using `npm install` or `yarn install` for yarn.

## Scripts

| Description 	| npm 	| yarn 	|
|--------------------------|--------------------|-----------------|
| Run Jest test 	| `npm run test` 	| `yarn test` 	|
| Run Jest with watch mode 	| `npm run test:watch` 	| `yarn test:watch` 	|
| Build to js files 	| `npm run build` 	| `yarn build` 	|
| Run example | `npm run start` 	| `yarn start` 	|
| Run example with watch mode | `npm run start:dev` 	| `yarn start:dev` 	|
| Run js builded files | `npm run start:prod` 	| `yarn start:prod` |
