import * as supertest from 'supertest'
import TestsController from '../TestsController';

const app = TestsController.express;

describe("HTTP Verbs - Methods Decorators", () => {
  describe("GET", () => {
    it("should be response hello world", async () => {
      const result = await supertest(app).get("/api/tests/get-hello-world");
      expect(result.status).toEqual(200);
      expect(JSON.parse(result.text)).toEqual('hello world');
    });

    it("should be response 404 when url not exits", async () => {
      const result = await supertest(app).get("/api/tests/random-url");
      expect(result.status).toEqual(404);
    });


    it("should be response 500 when are internal error", async () => {
      const result = await supertest(app).get("/api/tests/get-intentional-throw");
      expect(result.status).toEqual(500);
    });
  });

  describe("POST", () => {
    it("should be response hello world", async () => {
      const result = await supertest(app).post("/api/tests/post-hello-world");
      expect(result.status).toEqual(200);
      expect(JSON.parse(result.text)).toEqual('hello world');
    });

    it("should be response 404 when url not exits", async () => {
      const result = await supertest(app).post("/api/tests/random-url");
      expect(result.status).toEqual(404);
    });


    it("should be response 500 when are internal error", async () => {
      const result = await supertest(app).post("/api/tests/post-intentional-throw");
      expect(result.status).toEqual(500);
    });
  });

  describe("PUT", () => {
    it("should be response hello world", async () => {
      const result = await supertest(app).put("/api/tests/put-hello-world");
      expect(result.status).toEqual(200);
      expect(JSON.parse(result.text)).toEqual('hello world');
    });

    it("should be response 404 when url not exits", async () => {
      const result = await supertest(app).put("/api/tests/random-url");
      expect(result.status).toEqual(404);
    });


    it("should be response 500 when are internal error", async () => {
      const result = await supertest(app).put("/api/tests/put-intentional-throw");
      expect(result.status).toEqual(500);
    });
  });
});