import {
    Controller,
    Get,
    Post,
    Put,
    QueryParams,
    Body
} from '../src/core/decorators';
import app from '../src/core/express';
import { Router } from 'express';

@Controller({ path: 'tests' })
class TestsController {
    // GETS METHODS
    @Get('/get-hello-world')
    public helloWorld() {
        return 'hello world';
    }

    @Get('/get-intentional-throw')
    public intentionalThrow() {
        let hello: any;
        return hello.throw;
    }

    // POSTS METHODS
    @Post('/post-hello-world')
    public helloPost() {
        return 'hello world';
    }

    @Post('/post-intentional-throw')
    public intentionalThrowPost() {
        let hello: any;
        return hello.throw;
    }

    // PUTS METHODS
    @Put('/put-hello-world')
    public helloPut() {
        return 'hello world';
    }

    @Put('/put-intentional-throw')
    public intentionalThrowPut() {
        let hello: any;
        return hello.throw;
    }

    // QueryParams
    @Get('/get-query-params')
    public getQueryParams(@QueryParams queryParams: any) {
        return queryParams;
    }

    // Body
    @Post('/post-body')
    public getBody(@Body body: any) {
        return body;
    }
}

const test = new TestsController();
const router: Router = Router();

(test as any).setRouter(router);

export default app;
