import { json, urlencoded } from 'body-parser';

import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as methodOverride from 'method-override';

import { config } from './globals';

const BASE_PATH = config.application.basePath;

class App {

    get express(): express.Application {
        return this._app;
    }

    private _app: express.Application;

    constructor() {
        this._app = express();
    }

    start(): void {
        const healthcheck = require('express-healthcheck')(); // tslint:disable-line

        this._app
            .get(`${BASE_PATH}/health`, healthcheck);

        this.middleware();
        this._app
            .listen(config.application.port, () =>
                console.log(`server started on port ${config.application.port} cwd: ${process.cwd()}`)
            )
    }

    middleware(callbacks?: Middleware[] | Middleware): void {
        this._app
            .use(cookieParser())
            .use(methodOverride())
            .use(cors())
            .use(json())
            .use(urlencoded({ extended: true }));

        if (callbacks && callbacks.length > 0) {
            if (Array.isArray(callbacks)) {
                callbacks.forEach((callback) => {
                    this._app.use(callback);
                })
            } else {
                this._app.use(callbacks);
            }
        }
    }
}

type Middleware = (req: express.Request, res: express.Response, next: express.NextFunction) => void;

export default new App();
