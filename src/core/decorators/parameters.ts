export const QUERYPARAM_KEY = Symbol('query');
export const BODY_KEY = Symbol('body');
import 'reflect-metadata';

export function QueryParams(target: Object, propertyKey: string | symbol, parameterIndex: number) {
    let queryParams: number[] = Reflect.getOwnMetadata(QUERYPARAM_KEY, target, propertyKey) || [];
    queryParams.push(parameterIndex);
    queryParams.sort();
    Reflect.defineMetadata(QUERYPARAM_KEY, queryParams, target, propertyKey);
}

export function Body(target: Object, propertyKey: string | symbol, parameterIndex: number) {
    let body: number[] = Reflect.getOwnMetadata(BODY_KEY, target, propertyKey) || [];
    body.push(parameterIndex);
    body.sort();
    Reflect.defineMetadata(BODY_KEY, body, target, propertyKey);
}

export * from './param';
