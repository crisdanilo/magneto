export const PARAMETER_KEY = Symbol('params')

function UpdateParams(
    target: any,
    propertyKey: string | symbol,
    parameterIndex: number,
    name: string,
) {
    let queryParams = (
        Reflect.getOwnMetadata(
            PARAMETER_KEY,
            target,
            propertyKey,
        ) || []
    );

    const metaData = queryParams.find((query: { idx: number, name: string }) => query.idx === parameterIndex)

    if (!metaData) {
        queryParams.push({ idx: parameterIndex, name });
    } else {
        queryParams = { ...queryParams, ...{ idx: parameterIndex, name } };
    }

    Reflect.defineMetadata(
        PARAMETER_KEY,
        queryParams,
        target,
        propertyKey
    )
}

export function Param(name: string) {
    return (
        target: any,
        propertyKey: string | symbol,
        parameterIndex: number,
    ) => {

        UpdateParams(target, propertyKey, parameterIndex, name)

        const parameterMetaData = (
            Reflect.getOwnMetadata(
                PARAMETER_KEY,
                target,
                propertyKey) || []
        );

        Reflect.defineMetadata(
            PARAMETER_KEY,
            parameterMetaData,
            target,
            propertyKey,
        );
    }
}