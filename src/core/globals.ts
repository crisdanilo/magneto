import { path } from 'app-root-path';
import { existsSync, mkdirSync, appendFileSync } from 'fs';
import { logger } from '../libs/logger';

export const APP_ROOT = path;

if (!existsSync(`${APP_ROOT}/magneto.json`)) {
    logger.error('Cannot find the configuration file');
    appendFileSync(`${APP_ROOT}/magneto.json`, JSON.stringify({
        application: {
            root: './src',
            basePath: '/my-app-name',
            port: 5000,
        },
        controllers: './src/controllers'
    }, null, 4), 'utf8');
    logger.info('File created in the project root');
}

const appConfig = require(`${APP_ROOT}/magneto.json`);

if (appConfig.controllers && !existsSync(appConfig.controllers)) {
    mkdirSync(appConfig.controllers);
}

const CONFIG_KEY = Symbol.for("Magneto.Config");
const globalSymbols = Object.getOwnPropertySymbols(global);
const hasConfig = (globalSymbols.indexOf(CONFIG_KEY) > -1);

let basePath = '/';

if (appConfig.application.basePath) {
    basePath = appConfig.application.basePath.startsWith('/') ? appConfig.application.basePath : `/${appConfig.application.basePath}`
}

if (!hasConfig) {
    (global as any).CONFIG_KEY = {
        application: {
            outDir: appConfig.application.outDir || './build',
            root: appConfig.application.root || './src',
            basePath,
            port: appConfig.application.port || 5000,
        },
        controllers: appConfig.controllers || `${APP_ROOT}/${appConfig.root}/controllers`,
    };
}

const singleton = {};

Object.defineProperty(singleton, "instance", {
    get: function () {
        return (global as any).CONFIG_KEY;
    }
});

Object.freeze(singleton);

export const config: IConfig = (singleton as any).instance;

interface IApplication {
    root: string;
    basePath: string;
    port: number;
}

interface IConfig {
    application: IApplication;
    controllers: string;
}